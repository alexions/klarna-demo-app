package com.mycompany.app;

import com.klarna.rest.Client;
import com.klarna.rest.api.checkout.CheckoutOrdersApi;
import com.klarna.rest.http_transport.HttpTransport;

import com.klarna.rest.model.ApiException;

import com.klarna.rest.api.checkout.model.CheckoutAddress;
import com.klarna.rest.api.checkout.model.CheckoutMerchantUrls;
import com.klarna.rest.api.checkout.model.CheckoutOrder;
import com.klarna.rest.api.checkout.model.CheckoutOrderLine;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Klarna OM Example
 */
public class App
{
    public static void main( String[] args )
    {
        String username = "K123456_abcd12345";
        String password = "sharedSecret";

        Client client = new Client(username, password, HttpTransport.EU_TEST_BASE_URL);
        CheckoutOrdersApi checkoutOrdersApi = client.newCheckoutOrdersApi();

        final List<CheckoutOrderLine> lines = Arrays.asList(
                new CheckoutOrderLine()
                        .type("physical")
                        .reference("123050")
                        .name("Tomatoes")
                        .quantity(10L)
                        .quantityUnit("kg")
                        .unitPrice(600L)
                        .taxRate(2500L)
                        .totalAmount(6000L)
                        .totalTaxAmount(1200L),

                new CheckoutOrderLine()
                        .type("physical")
                        .reference("543670")
                        .name("Bananas")
                        .quantity(1L)
                        .quantityUnit("bag")
                        .unitPrice(5000L)
                        .taxRate(2500L)
                        .totalAmount(4000L)
                        .totalDiscountAmount(1000L)
                        .totalTaxAmount(800L)
        );

        final CheckoutMerchantUrls urls = new CheckoutMerchantUrls()
                .terms("http://www.example.com/toc")
                .checkout("http://www.example.com/checkout?klarna_order_id={checkout.order.id}")
                .confirmation("http://www.example.com/thank-you?klarna_order_id={checkout.order.id}")
                .push("http://www.example.com/create_order?klarna_order_id={checkout.order.id}");

        CheckoutOrder data = new CheckoutOrder()
                .purchaseCountry("GB")
                .purchaseCurrency("GBP")
                .locale("EN-GB")
                .orderAmount(10000L)
                .orderTaxAmount(2000L)
                .orderLines(lines)
                .merchantUrls(urls);

        try {
            CheckoutOrder order = checkoutOrdersApi.create(data);
            System.out.println(order);

        } catch (IOException e) {
            System.out.println("Connection problem: " + e.getMessage());
        } catch (ApiException e) {
            System.out.println(e.getMessage());
        }
    }
}
