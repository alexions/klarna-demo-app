Klarna Demo App
==============================

This demo app was created for a testing purposes. 

How to Run
==============================

Clone the repo: 
```bash
git clone git@bitbucket.org:alexions/klarna-demo-app.git
cd klarna-demo-app
```

Change the credentials and data here:
```bash
vim src/main/java/com/mycompany/app/App.java
...

```

Install the dependencies and run the app:
```bash
$ make

mvn install
[INFO] Scanning for projects...
[INFO]
[INFO] ----------------------< com.mycompany.app:my-app >----------------------
[INFO] Building my-app 1.0
[INFO] --------------------------------[ jar ]---------------------------------
[INFO]
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ my-app ---
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] Copying 1 resource
[INFO]
[INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ my-app ---
[INFO] Changes detected - recompiling the module!
[WARNING] File encoding has not been set, using platform encoding UTF-8, i.e. build is platform dependent!
[INFO] Compiling 1 source file to /home/user/my-app/target/classes
[INFO]
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ my-app ---
[WARNING] Using platform encoding (UTF-8 actually) to copy filtered resources, i.e. build is platform dependent!
[INFO] skip non existing resourceDirectory /home/user/my-app/src/test/resources
[INFO]
[INFO] --- maven-compiler-plugin:3.1:testCompile (default-testCompile) @ my-app ---
[INFO] No sources to compile
[INFO]
[INFO] --- maven-surefire-plugin:2.12.4:test (default-test) @ my-app ---
[INFO] No tests to run.
[INFO]
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ my-app ---
[INFO] Building jar: /home/user/my-app/target/my-app-1.0.jar
[INFO]
[INFO] --- maven-install-plugin:2.4:install (default-install) @ my-app ---
[INFO] Installing /home/user/my-app/target/my-app-1.0.jar to /home/user/.m2/repository/com/mycompany/app/my-app/1.0/my-app-1.0.jar
[INFO] Installing /home/user/my-app/pom.xml to /home/user/.m2/repository/com/mycompany/app/my-app/1.0/my-app-1.0.pom
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  2.592 s
[INFO] Finished at: 2018-12-03T17:28:05+01:00
[INFO] ------------------------------------------------------------------------
mvn exec:java -Dexec.mainClass="com.mycompany.app.App"
[INFO] Scanning for projects...
[INFO]
[INFO] ----------------------< com.mycompany.app:my-app >----------------------
[INFO] Building my-app 1.0
[INFO] --------------------------------[ jar ]---------------------------------
[INFO]
[INFO] --- exec-maven-plugin:1.6.0:java (default-cli) @ my-app ---

class OrderManagementCapture {
    captureId: xxxxxxx
    klarnaReference: yyyyyy-1
    capturedAmount: 6000
    capturedAt: 2018-08-09T13:49:40.658Z
    description: Shipped part of the order
    orderLines: [class OrderManagementOrderLine {
        reference: 123456
        .....

[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  2.391 s
[INFO] Finished at: 2018-12-03T17:28:09+01:00
[INFO] ------------------------------------------------------------------------
```

